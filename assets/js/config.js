var eventsFunctions = function(simulator) {
    $('body').on('click', '.btnCloseModal', function() {
        simulator.hideModal();
        return false;
    });

    $('body').on('click', 'a.addItem',function(){
        simulator.addItem($(this).attr('data-type'),true,$(this),simulator.currentSurface);
        return false;
    });

    $('#toggleMeasure').on('click',function(){
        simulator.toggleMeasure();
        return false;
    });

    $('.btnAction').on('click',function(){
        simulator.changePage( $(this).attr('data-page') );
        return false;
    });

    $('.navMove a').on('click',function(){
        if( !$(this).hasClass('disable') ){
            simulator.moveSurface($(this).attr('data-type'));
        } 
        return false;
    });

    $('body').on('keyup, click', 'input.measure',function(){
        simulator.setSliderValues($(this));
        return false;
    });

    $('body').on('change', '.mSlider',function(){
        obj = $(this);
        obj.closest('fieldset').find('input.measure').val($(this).val());
    });

    $('.directions a').on('click',function(){
        simulator.newDirection($(this));
        return false;
    });

    $("#itemList").on('change',function(){
        $('ul.prods').slideUp(400);
        simulator.selectItem();
    });

    $('.add_component').on('click',function(){
        $('ul.prods').slideToggle(400);
        return false;
    });

    $('a.delete_component').on('click',function(){
        simulator.countDelectableItems();
        return false;
    });

    $('#centerSurface').on('click',function(){
        simulator.centerSurfaceFnc();
        return false;
    });
}

var startValues = {
    'surfaceWidth':     "4000",
    'surfaceHeight':    "3000",
    'surfaceDepth':     "4000",
    'slidersMin':       "500",
    'slidersMax':       "10000",
    'slidersSteps':     "1",
    'slidersStartX':    "200",
    'slidersStartY':    "200",
    'zoom':{
        'from':         "25",
        'to':           "600",
        'step':         "1",
    }
}

var config = {
    'family':{
        'brightline': {
            'functions':    brightlineFunctions,
            'events':       eventsBLFunctions,
            'infoProd':{
                'l250':{
                    'A303-00-11-1': {'type':'BL','w':2.9,'k':3000,'lm':307,'eff':43,'l':250},
                    'A303-00-12-1': {'type':'BL','w':2.9,'k':4000,'lm':313,'eff':43,'l':250},
                    'A304-00-11-1': {'type':'BLHO','w':5.8,'k':3000,'lm':596,'eff':43,'l':250},
                    'A304-00-12-1': {'type':'BLHO','w':5.8,'k':4000,'lm':605,'eff':43,'l':250},
                },
                'l500':{
                    'A303-00-21-1': {'type':'BL','w':5.8,'k':3000,'lm':615,'eff':43,'l':500},
                    'A303-00-22-1': {'type':'BL','w':5.8,'k':4000,'lm':625,'eff':43,'l':500},
                    'A304-00-21-1': {'type':'BLHO','w':11.5,'k':3000,'lm':1191,'eff':43,'l':500},
                    'A304-00-22-1': {'type':'BLHO','w':11.5,'k':4000,'lm':1211,'eff':43,'l':500},
                },
                'l1000':{
                    'A303-00-31-1': {'type':'BL','w':11.5,'k':3000,'lm':1230,'eff':43,'l':1000},
                    'A303-00-32-1': {'type':'BL','w':11.5,'k':4000,'lm':1250,'eff':43,'l':1000},
                    'A304-00-31-1': {'type':'BLHO','w':23,'k':3000,'lm':2382,'eff':43,'l':1000},
                    'A304-00-32-1': {'type':'BLHO','w':23,'k':4000,'lm':2422,'eff':43,'l':1000},
                },
                'l2000':{
                    'A303-00-41-1': {'type':'BL','w':23,'k':3000,'lm':2460,'eff':43,'l':2000},
                    'A303-00-42-1': {'type':'BL','w':23,'k':4000,'lm':2500,'eff':43,'l':2000},
                    'A304-00-41-1': {'type':'BLHO','w':46,'k':3000,'lm':4764,'eff':43,'l':2000},
                    'A304-00-42-1': {'type':'BLHO','w':46,'k':4000,'lm':4844,'eff':43,'l':2000},
                },
                'l3000':{
                    'A303-00-51-1': {'type':'BL','w':34.5,'k':3000,'lm':3690,'eff':43,'l':3000},
                    'A303-00-52-1': {'type':'BL','w':34.5,'k':4000,'lm':3750,'eff':43,'l':3000},
                    'A304-00-51-1': {'type':'BLHO','w':69,'k':3000,'lm':7146,'eff':43,'l':3000},
                    'A304-00-52-1': {'type':'BLHO','w':69,'k':4000,'lm':7266,'eff':43,'l':3000},
                },      
            },
            'prodByPosi':{
                1:{
                    "c4":{"end":2,"next":4,"x":-8.5,"y":0,"nX":111,"nY":102.5,"dir":"h","spX":-8.5,"spY":0,"width":111,"height":111},
                    "lv":{"end":3,"next":1,"x":-8.5,"y":0,"nX":8.5,"nY":"L","dir":"v","spX":-8.5,"spY":0,"width":17,"height":"L"},
                    "c3":{"end":4,"next":2,"x":-102.5,"y":0,"nX":0,"nY":102.5,"dir":"h","spX":-102.5,"spY":0,"width":111,"height":111},
                    "s1":{"end":3,"next":1,"x":-8.5,"y":0,"nX":8.5,"nY":96,"dir":"v","spX":-8.5,"spY":0,"width":17,"height":96},
                },
                2:{
                    "c1":{"end":3,"next":1,"x":-111,"y":-8.5,"nX":8.5,"nY":111,"dir":"v","spX":-111,"spY":-8.5,"width":111,"height":111},
                    "lh":{"end":4,"next":2,"x":"-L","y":-8.5,"nX":0,"nY":8.5,"dir":"h","spX":"-L","spY":-8.5,"width":"L","height":17},
                    "c4":{"end":1,"next":3,"x":-111,"y":-102.5,"nX":8.5,"nY":0,"dir":"v","spX":-111,"spY":-102.5,"width":111,"height":111},
                    "s2":{"end":4,"next":2,"x":-96,"y":-8.5,"nX":0,"nY":8.5,"dir":"h","spX":-96,"spY":-8.5,"width":96,"height":17},
                },
                3:{
                    "c2":{"end":4,"next":2,"x":-102.5,"y":-111,"nX":0,"nY":8.5,"dir":"h","spX":-102.5,"spY":-111,"width":111,"height":111},
                    "lv":{"end":1,"next":3,"x":-8.5,"y":"-L","nX":8.5,"nY":0,"dir":"v","spX":-8.5,"spY":"-L","width":17,"height":"L"},
                    "c1":{"end":2,"next":4,"x":-8.5,"y":-111,"nX":111,"nY":8.5,"dir":"h","spX":-8.5,"spY":-111,"width":111,"height":111},
                    "s3":{"end":1,"next":3,"x":-8.5,"y":-96,"nX":8.5,"nY":0,"dir":"v","spX":-8.5,"spY":-96,"width":17,"height":96},
                },
                4:{
                    "c3":{"end":1,"next":3,"x":0,"y":-102.5,"nX":102.5,"nY":0,"dir":"v","spX":0,"spY":-102.5,"width":111,"height":111},
                    "lh":{"end":2,"next":4,"x":0,"y":-8.5,"nX":"L","nY":8.5,"dir":"h","spX":0,"spY":-8.5,"width":"L","height":17},
                    "c2":{"end":3,"next":1,"x":0,"y":-8.5,"nX":102.5,"nY":111,"dir":"v","spX":0,"spY":-8.5,"width":111,"height":111},
                    "s4":{"end":2,"next":4,"x":0,"y":-8.5,"nX":96,"nY":8.5,"dir":"h","spX":0,"spY":-8.5,"width":96,"height":17},
                }
            },
            'componentList':{
                'typeS':{
                    'dir':'v',
                    'items':{
                        's1':{'lclass':'item_10','start':'1','class':'addItem','text':'Esq. S1'},
                        's2':{'lclass':'item_11','start':'2','class':'addItem','text':'Esq. S2'},
                        's3':{'lclass':'item_12','start':'3','class':'addItem','text':'Esq. S3'},
                        's4':{'lclass':'item_13','start':'4','class':'addItem','text':'Esq. S4'}
                    }
                },
                'typeL':{
                    'dir':'v',
                    'items':{
                        'l250':{'lclass':'item_01','start':false,'class':'addItem','text':'250mm'},
                        'l500':{'lclass':'item_02','start':false,'class':'addItem','text':'500mm'},
                        'l1000':{'lclass':'item_03','start':false,'class':'addItem','text':'1000mm'},
                        'l2000':{'lclass':'item_04','start':false,'class':'addItem','text':'2000mm'},
                        'l3000':{'lclass':'item_05','start':false,'class':'addItem','text':'3000mm'},
                    }
                },
                'typeC':{
                    'dir':'v',
                    'items':{
                        'c1':{'lclass':'item_06','start':false,'class':'addItem addCorner','text':'Esq. c1'},
                        'c2':{'lclass':'item_07','start':false,'class':'addItem addCorner','text':'Esq. c2'},
                        'c4':{'lclass':'item_08','start':false,'class':'addItem addCorner','text':'Esq. c4'},
                        'c3':{'lclass':'item_09','start':false,'class':'addItem addCorner','text':'Esq. c3'},
                    }
                }
            }
        },
        'blackfoster':{
            'functions':    blackfosterFunctions,
            'events':       eventsBKFunctions,
            'infoProd':{
            },
            'prodByPosi':{
                1:{
                    "lv":{"end":3,"next":1,"x":-9.7,"y":0,"nX":9.7,"nY":"L","dir":"v","spX":-9.7,"spY":0,"width":19.4,"height":"L"},
                    "gv":{"end":3,"next":1,"x":-9.7,"y":0,"nX":9.7,"nY":"L","dir":"v","spX":-9.7,"spY":0,"width":19.4,"height":"L"},
                },
                2:{
                    "lh":{"end":4,"next":2,"x":"-L","y":-9.7,"nX":0,"nY":9.7,"dir":"h","spX":"-L","spY":-9.7,"width":"L","height":19.4},
                    "gh":{"end":4,"next":2,"x":"-L","y":-9.7,"nX":0,"nY":9.7,"dir":"h","spX":"-L","spY":-9.7,"width":"L","height":19.4},
                },
                3:{
                    "lv":{"end":1,"next":3,"x":-9.7,"y":"-L","nX":9.7,"nY":0,"dir":"v","spX":-9.7,"spY":"-L","width":19.4,"height":"L"},
                    "gv":{"end":1,"next":3,"x":-9.7,"y":"-L","nX":9.7,"nY":0,"dir":"v","spX":-9.7,"spY":"-L","width":19.4,"height":"L"},
                },
                4:{
                    "lh":{"end":2,"next":4,"x":0,"y":-9.7,"nX":"L","nY":9.7,"dir":"h","spX":0,"spY":-9.7,"width":"L","height":19.4},
                    "gh":{"end":2,"next":4,"x":0,"y":-9.7,"nX":"L","nY":9.7,"dir":"h","spX":0,"spY":-9.7,"width":"L","height":19.4},
                }
            },
            'componentList':{
                'typeL':{
                    'dir':'h',
                    'items':{
                        'l87':{'lclass':'item_01','start':false,'class':'addItem','text':'(x2) 87mm'},
                        'l130':{'lclass':'item_02','start':false,'class':'addItem','text':'(x3) 130mm'},
                        'l216':{'lclass':'item_03','start':false,'class':'addItem','text':'(x5) 216mm'},
                        'l431':{'lclass':'item_04','start':false,'class':'addItem','text':'(x10) 431mm'},
                        'l646':{'lclass':'item_05','start':false,'class':'addItem','text':'(x15) 646mm'},
                    }
                },
                'typeG':{
                    'dir':'h',
                    'items':{
                        'g250':{'lclass':'item_06','start':false,'class':'addItem','text':'GAP 250'},
                        'g500':{'lclass':'item_07','start':false,'class':'addItem','text':'GAP 500'},
                        'g1000':{'lclass':'item_08','start':false,'class':'addItem','text':'GAP 1000'},
                        'g2000':{'lclass':'item_09','start':false,'class':'addItem','text':'GAP 2000'},
                    }
                }
            }
        },
        'fifty':{
            'functions':    fiftyFunctions,
            'events':       eventsFFunctions,
            'infoProd':{
            },
            'prodByPosi':{
                1:{
                    "lv":{"end":3,"next":1,"x":-7.2,"y":0,"nX":7.2,"nY":"L","dir":"v","spX":-7.2,"spY":0,"width":14.4,"height":"L"},
                },
                2:{
                    "lh":{"end":4,"next":2,"x":"-L","y":-7.2,"nX":0,"nY":7.2,"dir":"h","spX":"-L","spY":-7.2,"width":"L","height":14.4},
                },
                3:{
                    "lv":{"end":1,"next":3,"x":-7.2,"y":"-L","nX":7.2,"nY":0,"dir":"v","spX":-7.2,"spY":"-L","width":14.4,"height":"L"},
                },
                4:{
                    "lh":{"end":2,"next":4,"x":0,"y":-7.2,"nX":"L","nY":7.2,"dir":"h","spX":0,"spY":-7.2,"width":"L","height":14.4},
                }
            },
            'componentList':{
                 'typeL':{
                    'dir':'h',
                    'items':{
                        'l880':{'lclass':'item_01','start':false,'class':'addItem','text':'90mm'},
                        'l1164':{'lclass':'item_02','start':false,'class':'addItem','text':'120mm'},
                        'l1448':{'lclass':'item_03','start':false,'class':'addItem','text':'150mm'},
                        'l2868':{'lclass':'item_04','start':false,'class':'addItem','text':'300mm'},
                    }
                }
            }
        }    
    },
    'zoom':{
        from:           startValues.zoom.from,
        to:             startValues.zoom.to,
        step:           startValues.zoom.step,
        format:         '%s%',
        showScale:      false,
        width:          300,
        showLabels:     true,
        snap:           true,
        onstatechange:  function(){
            simulator.setScale();
        }
    },
    'sliders':{
        from:           startValues.slidersMin,
        to:             startValues.slidersMax,
        step:           startValues.slidersSteps,
        format:         '%s',
        width:          140,
        showLabels:     true,
        showScale:      false,
        snap:           true,
        onstatechange:  function(){
            simulator.changeSurfaceMeasure();
        }
    },
    'slidersFields':{
        'width':    {
            'legend':   'textAnchoLegend',
            'class':    'measure-slider mSlider',
            'value':    startValues.surfaceWidth,
            'min':      startValues.slidersMin,
            'max':      startValues.slidersMax,
            'step':     startValues.slidersSteps
        },
        'height':   {
            'legend':   'textAlturaLegend',
            'class':    'measure-slider mSlider',
            'value':    startValues.surfaceHeight,
            'min':      startValues.slidersMin,
            'max':      startValues.slidersMax,
            'step':     startValues.slidersSteps
        },
        'startX':    {
            'legend':   'textXmargin',
            'class':    'legend-sliderY mSlider',
            'value':    startValues.slidersStartX,
            'min':      0,
            'max':      startValues.slidersMax,
            'step':     startValues.slidersSteps
        },
        'startY':    {
            'legend':   'textYmargin',
            'class':    'legend-sliderY mSlider',
            'value':    startValues.slidersStartY,
            'min':      0,
            'max':      startValues.slidersMax,
            'step':     startValues.slidersSteps
        }
    }
}