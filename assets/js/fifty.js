var fiftyFunctions  = new function() {
    this.lineName           = 'Fifty';
    this.temperature        = false;
    this.fiftyType          = false;
    this.rectasArray        = ['l880','l1164','l1448','l2868'];
    this.rectasSizes        = {232.8:'l1164',289.6:'l1448',573.6:'l2868'};
    this.surfaceTypes       = {
        'F':{
            'id':           'back_surface',
            'name':         'Pared del fondo',
            'connectTo':    {},
            'measure':      {'w':'w','h':'h'}
        },
        'T':{
            'id':           'roof_surface',
            'name':         'Techo',
            'connectTo':    {},
            'measure':      {'w':'w','h':'d'}
        }
    }

    this.startFifty   = function(){
        var connect         = this.returnConnectionArray('0000');
        this.fiftyType      = $('body').attr('data-family');

        if(this.fiftyType == 'wall'){
            var getSurface      = 'F';
            var thisVals        = {
                value: '',
                text : 'Pared'
            };
        } else {
            var getSurface      = 'T';
            var thisVals        = {
                value: '',
                text : 'Techo'
            };
        }
        $('#surfaceList').append($('<option>',thisVals));
        this.addSurface(getSurface,connect);
    }

    this.toggleComponents = function(){
        //TODO
    }

    this.setParam           = function(type,obj){
        this[type]          = obj.val();
        $('.page.pageActive a.disabled').removeClass('disabled');
    }

    this.addItemByLine = function(IObj){
        IObj.straightType       = 'l';
        IObj.data               = this.prods[IObj.sP]['l'+IObj.dir];
        IObj.large              = 'pendiente';

        IObj.large              = this.mmToPx( parseFloat( IObj.type.substring(1,IObj.type.length) ) );

        if(IObj.total == 0){
            IObj.stPos                          = {
                'x': this.getLarge(IObj.data.spX,IObj.large),
                'y': this.getLarge(IObj.data.spY,IObj.large)
            }
        } else {
            IObj.stPos                          = {
                'x': parseFloat(IObj.currentLine.nextX) + this.getLarge(IObj.data.x,IObj.large),
                'y': parseFloat(IObj.currentLine.nextY) + this.getLarge(IObj.data.y,IObj.large)
            } 
        }

        IObj.X                                  = parseFloat(IObj.stPos.x) + this.getLarge(IObj.data.nX,IObj.large);
        IObj.Y                                  = parseFloat(IObj.stPos.y) + this.getLarge(IObj.data.nY,IObj.large);
        IObj.html                               = '<div data-posi="'+IObj.posi+'" id="' + IObj.objectID + '" data-label="'+ IObj.itemID +'" class="prodItem'+IObj.clase+' '+IObj.type+'" style="top:'+IObj.stPos.y+'px;left:'+IObj.stPos.x+'px"></div>';

        if (IObj.currentLine.rectas.type != IObj.straightType){
            IObj.currentLine.rectas.type        = IObj.straightType;
            IObj.currentLine.rectas.items = [];
        }
        IObj.currentLine.rectas.items.push(IObj.large);
        console.log(IObj.currentLine.rectas);
        return IObj;
    }
}
var eventsFFunctions = function(simulator) {
    $('.selectByList form input').on('click',function(){
        var param = $(this).closest('form').attr('data-variable');
        simulator.setParam(param,$(this));
    });

    $('#KitBtnF').on('click',function(){
        simulator.startFifty();
    });
}