var blackfosterFunctions = new function() {
    this.lineName           = 'Black Foster';
    this.temperature        = false;
    this.driver             = false;
    this.mountingKit        = false;
    this.rectasArray        = ['l87','l130','l216','l431','l646','g500','g1000','g2000'];
    this.rectasSizes        = {26:'l130',43:'l216',86:'l431',129:'l646',100:'g500',200:'g1000',400:'g2000'};
    this.factorStraight     = 0.2;
    this.surfaceTypes       = {
        'F':{
            'id':           'back_surface',
            'name':         'Pared del fondo',
            'connectTo':    {},
            'measure':      {'w':'w','h':'h'}
        }
    }

    this.setParam           = function(type,obj){
        this[type]          = obj.val();
        $('.page.pageActive a.disabled').removeClass('disabled');
    }

    this.processMeasureStraight = function(type,number,plusLess){
        if(type != 'g'){
            return number + (this.factorStraight * plusLess);
        } else {
            return number;
        }
    }

    this.startBlackFoster   = function(){
        var connect         = this.returnConnectionArray('0000');
        var getSurface      = 'F';

        var thisVals        = {
            value: '',
            text : 'Pared'
        };
        $('#surfaceList').append($('<option>',thisVals));
        this.addSurface(getSurface,connect);
    }

    this.toggleComponents = function(){
        //TODO
    }

    this.addItemByLine = function(IObj){
        IObj.straightType       = 'l';
        if(IObj.type == 'g250' || IObj.type == 'g500' || IObj.type == 'g1000' || IObj.type == 'g2000'){
            IObj.data           = this.prods[IObj.sP]['g'+IObj.dir];
            IObj.clase          = ' gap gap'+(IObj.clase.replace(' ',''))+IObj.clase;
            IObj.straightType   = 'g';
        } else {
            IObj.data           = this.prods[IObj.sP]['l'+IObj.dir];
            IObj.large          = 'pendiente';
        }

        IObj.large              = this.mmToPx( parseFloat( IObj.type.substring(1,IObj.type.length) ) );

        if(IObj.total == 0){
            IObj.stPos                          = {
                'x': this.getLarge(IObj.data.spX,IObj.large),
                'y': this.getLarge(IObj.data.spY,IObj.large)
            }
        } else {
            IObj.stPos                          = {
                'x': parseFloat(IObj.currentLine.nextX) + this.getLarge(IObj.data.x,IObj.large),
                'y': parseFloat(IObj.currentLine.nextY) + this.getLarge(IObj.data.y,IObj.large)
            } 
        }

        IObj.X                                  = parseFloat(IObj.stPos.x) + this.getLarge(IObj.data.nX,IObj.large);
        IObj.Y                                  = parseFloat(IObj.stPos.y) + this.getLarge(IObj.data.nY,IObj.large);
        IObj.html                               = '<div data-posi="'+IObj.posi+'" id="' + IObj.objectID + '" data-label="'+ IObj.itemID +'" class="prodItem'+IObj.clase+' '+IObj.type+'" style="top:'+IObj.stPos.y+'px;left:'+IObj.stPos.x+'px"></div>';

        if (IObj.currentLine.rectas.type != IObj.straightType){
            IObj.currentLine.rectas.type        = IObj.straightType;
            IObj.currentLine.rectas.items = [];
        }
        IObj.currentLine.rectas.items.push(IObj.large);
        console.log(IObj.currentLine.rectas);
        return IObj;
    }
}
var eventsBKFunctions = function(simulator) {
    $('.selectByList form input').on('click',function(){
        var param = $(this).closest('form').attr('data-variable');
        simulator.setParam(param,$(this));
    });

    $('#KitBtnBF').on('click',function(){
        simulator.startBlackFoster();
    });
}