var brightlineFunctions = new function() {
    this.lineName       = 'Bright Line';
    this.rectasArray    = ['l250','l500','l1000','l2000','l3000'];
    this.rectasSizes    = { 100:'l500',200:'l1000',400:'l2000',600:'l3000' };
	this.surfaceTypes 	= {
        'I':{
            'id':           'left_surface',
            'name':         'Pared izquierda',
            'connectTo':    {'T':4,'F':4},
            'measure':      {'w':'d','h':'h'}
        },
        'D':{
            'id':           'right_surface',
            'name':         'Pared derecha',
            'connectTo':    {'F':2,'T':2},
            'measure':      {'w':'d','h':'h'}
        },
        'F':{
            'id':           'back_surface',
            'name':         'Pared del fondo',
            'connectTo':    {'I':2,'T':3,'D':4},
            'measure':      {'w':'w','h':'h'}
        },
        'T':{
            'id':           'roof_surface',
            'name':         'Techo',
            'connectTo':    {'I':1,'F':1,'D':1},
            'measure':      {'w':'w','h':'d'}
        }
    }

    this.processMeasureStraight = function(type,number,plusLess){
        return number;
    }

    this.changeSurfaceTab = function(current){
        $('.selectSurfacesGroup li, #selectSurfaceForm fieldset').removeClass('active');
        current.closest('li').addClass('active');
        var newFieldsetActive = $('#'+current.attr('data-type'));
        newFieldsetActive.addClass('active');
    }

    this.setSurfaceGroup = function(){
        var objThis     = this;
        var current     = $('.addSurfaceType:checked').val();
        var item        = $('.addSurfaceType:checked').closest('label');
        var parent      = $('.addSurfaceType:checked').closest('fieldset');
        var connect     = $('.addSurfaceType:checked').attr('data-connect');
        var currentText = item.find('span').html();
        connect         = connect.split('|'); 
        var count       = parent.attr('data-count');
        var types       = this.surfaceTypes;
        $('#surfaceList option').remove();
        $('#simulator .wrap').html('');
        $('#selectSurfaceForm label').removeClass('active');
        objThis.currentSurface  = false;
        objThis.surface         = [];
        
        switch(count){
            case "1":
                connect         = this.returnConnectionArray(connect[0]);
                var getSurface  = current.replace('single','');
                var thisVals = {
                    value: '',
                    text : getSurface == 'F' ? 'Pared' : 'Techo'
                };
                $('#surfaceList').append($('<option>',thisVals));
                objThis.addSurface(getSurface,connect);
            break;

            case "2":
            case "3":
                $('.wrapSimulator').addClass('view3D');
                var getSurface  = current.replace('multi','');
                var items       = getSurface.split("");
                var total       = 0;
                Object.keys(items).forEach(function(key) {
                    var Currentconnect      = objThis.returnConnectionArray(connect[total]);
                    var thisVals = {
                        value: types[items[key]].id,
                        text : types[items[key]].name
                    };
                    objThis.addSurface(items[key],Currentconnect);
                    $('#surfaceList').append($('<option>',thisVals));
                    total++;
                });  
            break;
        }
        $('p.surfaceSelectedText').html(currentText);
        $('.page.pageActive a.disabled').removeClass('disabled');
        item.addClass('active');
    }

    this.changeActiveSurface = function(current){
        $('#simulator .surface').removeClass('active');
        var newSurface      = current.val();
        this.currentSurface = newSurface;
        var objSurfCurrent  = this.surface[newSurface];

        $('#'+newSurface).addClass('active');
        $('#height, #heightControl').val(objSurfCurrent.height);
        $('#width, #widthControl').val(objSurfCurrent.width);
        $('#startX, #startXControl').val(objSurfCurrent.x);
        $('#startY, #startYControl').val(objSurfCurrent.y);
        this.deletingFunctions();
        this.centerSurfaceFnc();
        this.changeSurfaceMeasure();
        //this.updateAdjoiningWalls();
    }

    this.toggleComponents = function(){
        var connections         = this.surface[this.currentSurface].connect;
        var currentLine         = this.getCurrentLine();
        var total               = Object.keys(currentLine.items).length;
        var surfaceCLasses 		= {'1':'top_conection','2':'right_conection','3':'bottom_conection','4':'left_conection'};
        var jQSurface 			= $('.wrap');
        var types 				= this.surfaceTypes;
        var objThis 			= this;

        $('.addCorner, .typeS a').addClass('disabled').show().removeAttr('data-connect').removeAttr('data-canconnect');
        jQSurface.removeClass('top_conection').removeClass('right_conection').removeClass('bottom_conection').removeClass('left_conection');

        Object.keys(connections).forEach(function(key) {
            if(connections[key] == 0){
                $('.typeS a[data-start='+key+']').hide();
            } else {
                jQSurface.addClass(surfaceCLasses[key]);
                var checkSurface = types[connections[key]].id;
                var status = objThis.canConnectSurface(checkSurface);
                $('.typeS a[data-start='+key+']').attr('data-connect',connections[key]).attr('data-canconnect',status);
            }
        });

        if(total == 0){
            $('.typeS a[data-start='+currentLine.startPosi+']').removeClass('disabled');
        }

        Object.keys(this.prods[currentLine.startPosi]).forEach(function(key) {
            $('ul.prods li.typeC a[data-type='+key+']').removeClass('disabled');
        });
        $('ul.prods').attr('data-dir',currentLine.direction);
    }

    this.canConnectSurface = function(line){
        var surface         = this.surface[line];
        var total           = 0;
        var returnStatus 	= false;
        if (typeof(surface) != "undefined"){
            var currentLine     = surface.lines[surface.currentLine];
            var items           = currentLine.items;
            total               = Object.keys(items).length;
            if(total == 0){
            	returnStatus 	= true;
            } else {
            	returnStatus 	= false;
            }
        }
        return returnStatus;
    }

    this.setOppositeWallStartPositions  = function(data,create){
        var targetLine          = this.surface[data.surface].lines[this.surface[data.surface].currentLine];
        var startXcurrent       = this.surface[this.currentSurface].x;
        var startYcurrent       = this.surface[this.currentSurface].y;

        if(create){
            targetLine.direction    = data.dir;
            targetLine.startPosi    = parseFloat(data.sp);
            this.surface[data.surface].connectedTo = data.thisSurfaceType;
        }

        if(data.sameSV){
            if(data.dir == 'h'){
                targetLine.y        = this.surface[data.surface].y =  startYcurrent;
                if(data.sp==2){
                    targetLine.x    = this.surface[data.surface].x =  this.surface[data.surface].width;
                }
            } else {
                targetLine.x        = this.surface[data.surface].x =  startXcurrent;
                if(data.sp==3){
                    targetLine.y    = this.surface[data.surface].y =  this.surface[data.surface].height;
                }         
            }
        } else {
           if(data.dir == 'h'){
                targetLine.y        = this.surface[data.surface].y =  startXcurrent;
            } else {
                targetLine.x        = this.surface[data.surface].x =  startYcurrent;
                if(data.sp==1){
                    targetLine.x    = this.surface[data.surface].x =  this.surface[data.surface].width - startYcurrent;
                }
            }
        }
    }

    this.getOppositeLineStartData = function(obj,addElement){
        var currentSurfaceType  = obj.surface['type'];
        var oppositeSP          = this.surfaceTypes[currentSurfaceType].connectTo[obj.connect];
        var dir                 = oppositeSP%2 == 0 ? 'h' : 'v';
        var sameSlidersValues   = obj.dir == dir ? true : false;

        var sendData            = {
            'sp':               oppositeSP,
            'dir':              dir,
            'item':             's'+ oppositeSP,
            'sameSV':           sameSlidersValues,
            'surface':          this.surfaceTypes[obj.connect].id,
            'thisSurfaceType':  currentSurfaceType
        }
        
        this.setOppositeWallStartPositions(sendData,addElement);
        if(addElement){
            this.addItem(sendData.item,false,false,sendData.surface); 
        }
    }

    this.updateAdjoiningWalls = function(){
        var objThis         = this;
        var targetSurface   = objThis.surface[objThis.currentSurface];
        var currentW        = targetSurface.measureW;
        var currentH        = targetSurface.measureH;
        var totalSurfaces   = Object.keys(objThis.surface).length;
        var connectedTo     = targetSurface.connectedTo;
        if(totalSurfaces > 1){
            Object.keys(objThis.surface).forEach(function loop(key){
                //Updating width in all Adjoining Walls less the current wall - (SAME AXIS)
                if(currentW == objThis.surface[key].measureW && objThis.currentSurface != key){
                    objThis.surface[key].width = targetSurface.width.toString();
                }

                //Updating height in all Adjoining Walls less the current wall - (SAME AXIS)
                if(currentH == objThis.surface[key].measureH && objThis.currentSurface != key){
                    objThis.surface[key].height = targetSurface.height.toString();
                }

                //Updating width in all Adjoining Walls less the current wall - (DIFFERENT AXIS)
                if(currentW == objThis.surface[key].measureH && objThis.currentSurface != key){
                    objThis.surface[key].height = targetSurface.width.toString();
                }

                //Updating width in all Adjoining Walls less the current wall - (DIFFERENT AXIS)
                if(currentH == objThis.surface[key].measureW && objThis.currentSurface != key){
                    objThis.surface[key].width = targetSurface.height.toString();
                }

                if(connectedTo && objThis.surfaceTypes[connectedTo].id == key && objThis.currentSurface != key){
                    var line            = targetSurface.currentLine;
                    var dir             = targetSurface.lines[line].direction;
                    var SendData        = {
                        'surface':      targetSurface,
                        'connect':      connectedTo,
                        'dir':          dir
                    }                    
                    objThis.getOppositeLineStartData(SendData,false);
                }
            });
        }
    }

    this.addItemByLine = function(IObj){
        IObj.obj.polarity       = IObj.currentLine.polarity; 

        if(IObj.type == 'l250' || IObj.type == 'l500' || IObj.type == 'l1000' || IObj.type == 'l2000' || IObj.type == 'l3000'){
            IObj.data        = this.prods[IObj.sP]['l'+IObj.dir];
        } else {
            IObj.data        = this.prods[IObj.sP][IObj.type];
        }

        switch(IObj.type){
            case 'c1':
            case 'c2':
            case 'c3':
            case 'c4':
                if(IObj.total == 0){
                    IObj.stPos = {
                        'x': parseFloat(IObj.data.spX),
                        'y': parseFloat(IObj.data.spY)
                    }
                } else {
                    IObj.stPos = {
                        'x': parseFloat(IObj.currentLine.nextX) + parseFloat(IObj.data.x),
                        'y': parseFloat(IObj.currentLine.nextY) + parseFloat(IObj.data.y)
                    }
                }

                IObj.X                          = parseFloat(IObj.stPos.x) + parseFloat(IObj.data.nX);
                IObj.Y                          = parseFloat(IObj.stPos.y) + parseFloat(IObj.data.nY);

                IObj.html                       = '<div data-posi="'+IObj.posi+'" id="' + IObj.objectID + '" data-label="'+ IObj.itemID +'" class="prodItem corner '+IObj.type+'" style="top:'+IObj.stPos.y+'px;left:'+IObj.stPos.x+'px"></div>';
                IObj.currentLine.rectas.items   = [];
                IObj.currentLine.rectas.type    = "";

                IObj.clase                   = '';
                switch(IObj.currentLine.lastItem){
                    case 'c1':
                        if(IObj.type == 'c3'){IObj.changePolarity = true;}
                    break;

                    case 'c2':
                        if(IObj.type == 'c4'){IObj.changePolarity = true;}
                    break;

                    case 'c3':
                        if(IObj.type == 'c1'){IObj.changePolarity = true;}
                    break;

                    case 'c4':
                        if(IObj.type == 'c2'){IObj.changePolarity = true;}
                    break;
                }
                if(IObj.changePolarity){
                    IObj.obj.polarity                    = IObj.obj.polarity == 'A' ? 'B' : 'A';
                    IObj.currentLine.polarity            = IObj.obj.polarity;
                }
            break;

            case 's1':
            case 's2':
            case 's3':
            case 's4':
                if(IObj.SurfaceID == this.currentSurface){
                    IObj.connectTo                      = IObj.linkObj.attr('data-connect');
                    IObj.sameSurface                    = true;
                    IObj.cancconect                     = IObj.linkObj.attr('data-canconnect');
                } else {
                    IObj.sameSurface                    = false;
                    IObj.cancconect                     = false;
                }
                
                if( IObj.cancconect === 'false' && IObj.sameSurface){
                    IObj.canAdd                         = false;
                    IObj.errorText                      = this.getTranslate('msgNoConnect');
                    IObj.errorText                      = IObj.errorText.replace('[nombre]',this.surfaceTypes[IObj.connectTo].name);
                    this.showModal(IObj.errorText,'alert');
                } else {
                    if(IObj.sameSurface){
                        IObj.addOpposite                = true;
                        IObj.thisSurface.connectedTo    = IObj.connectTo;
                    }

                    this.disableSliders(IObj.type,IObj.SurfaceID);
                    if(IObj.total == 0){
                        IObj.stPos                      = {
                            'x': parseFloat(IObj.data.spX),
                            'y': parseFloat(IObj.data.spY)
                        }
                    } else {
                        IObj.stPos = {
                            'x': parseFloat(IObj.currentLine.nextX) + parseFloat(IObj.data.x),
                            'y': parseFloat(IObj.currentLine.nextY) + parseFloat(IObj.data.y)
                        }
                    }

                    IObj.X                              = parseFloat(IObj.stPos.x) + parseFloat(IObj.data.nX);
                    IObj.Y                              = parseFloat(IObj.stPos.y) + parseFloat(IObj.data.nY);

                    IObj.html                           = '<div data-posi="'+IObj.posi+'" id="' + IObj.objectID + '" data-label="'+ IObj.itemID +'" class="prodItem'+IObj.clase+' cornerS '+IObj.type+'" style="top:'+IObj.stPos.y+'px;left:'+IObj.stPos.x+'px"></div>';
                    IObj.currentLine.rectas.items       = [];
                    IObj.currentLine.rectas.type        = "";
                    IObj.currentLine.starJoin           = IObj.sP;
                }
            break;

            default:
                IObj.large                              = this.mmToPx( parseFloat( IObj.type.substring(1,IObj.type.length) ) );

                if(IObj.total == 0){
                    IObj.stPos                          = {
                        'x': this.getLarge(IObj.data.spX,IObj.large),
                        'y': this.getLarge(IObj.data.spY,IObj.large)
                    }
                } else {
                    IObj.stPos                          = {
                        'x': parseFloat(IObj.currentLine.nextX) + this.getLarge(IObj.data.x,IObj.large),
                        'y': parseFloat(IObj.currentLine.nextY) + this.getLarge(IObj.data.y,IObj.large)
                    } 
                }

                IObj.X                                  = parseFloat(IObj.stPos.x) + this.getLarge(IObj.data.nX,IObj.large);
                IObj.Y                                  = parseFloat(IObj.stPos.y) + this.getLarge(IObj.data.nY,IObj.large);
                IObj.html                               = '<div data-posi="'+IObj.posi+'" id="' + IObj.objectID + '" data-label="'+ IObj.itemID +'" class="prodItem'+IObj.clase+' straight '+IObj.type+'" style="top:'+IObj.stPos.y+'px;left:'+IObj.stPos.x+'px"></div>';
                IObj.currentLine.rectas.type        = 'l';
                IObj.currentLine.rectas.items.push(IObj.large);
                console.log(IObj.currentLine.rectas);
            break;
        }
        return IObj;
    }
}

var eventsBLFunctions = function(simulator) {
    $('.selectSurfacesGroup li a').on('click',function(){
        simulator.changeSurfaceTab($(this));
        return false;
    });

    $('.addSurfaceType').on('click',function(){
        simulator.setSurfaceGroup();
    });

    $('#surfaceList').on('change',function(){
        simulator.changeActiveSurface($(this));
    });
}