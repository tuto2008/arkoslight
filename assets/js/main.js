$(window).resize(function(){
	simulator.browserAdjust(simulator);
});
$(window).bind('orientationchange', function(){
	simulator.browserAdjust(simulator);
});

$(document).ready(function(){
	if($('body').attr('id') != 'home'){
		var current = $('body').attr('id');
		simulator.start( config.family[ current ], current );
		simulator.changePage('page_start_'+current);
		simulator.startSliders();
	}
	simulator.traduceTemplate();
	simulator.browserAdjust(simulator);

	//hola2
});