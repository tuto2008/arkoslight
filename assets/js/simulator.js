var simulator 			= new function() {
    this.page                   = "page_selectLine";
    this.zoom                   = 100;
    this.posiX                  = 0;
    this.posiY                  = 0;
    this.unit                   = " mm";
    this.scalePix               = 0.2;
    this.scaleMm                = 5;
    this.surface                = [];
    this.currentSurface         = false;
    this.currentX 		        = 0;
    this.currentY 		        = 0;
    this.grid                   = true;
    this.prods                  = {};
    this.productLine            = '';
    this.lineID                 = false;
    this.lang                   = 'es';
    this.translation            = translations[this.lang];

    this.start                  = function(productLine,lineID){
    	$('input#zoomScale').val(this.zoom);
        this.lineID             = lineID;
        this.grid               = false;
        this.productLine        = productLine;
        this.prods              = this.productLine.prodByPosi;
        $.extend( true, this, this.productLine.functions );
        $.extend( true, eventsFunctions(this), this.productLine.events(this) );
        //$.extend( true, this, eventsFunctions(this) );
        this.toggleMeasure();
        $('#page_simulator h1').html(this.lineName);
        this.createComponentsBtns();
        $('#page_simulator').addClass(lineID);
    }

    this.createComponentsBtns = function(){
        var buttons     = this.productLine.componentList;
        var target      = $('#listComponentsBtns');
        target.html('').attr('class',this.lineID);
        Object.keys(buttons).forEach(function(key) {
            var thisId      = 'group_'+key;
            var currentObj  = buttons[key];
            target.append('<ul id="'+thisId+'" class="prods" data-dir="'+currentObj.dir+'"></ul>');
            Object.keys(currentObj.items).forEach(function(item) {
                var currentItem  = currentObj.items[item];
                var startP = currentItem.start ? ' data-start="'+currentItem.start+'"': '';
                $('#'+thisId).append('<li class="'+currentItem.lclass+' '+key+'"><a href="#"'+startP+' data-type="'+item+'" class="'+currentItem.class+'">'+currentItem.text+'</a></li>');
            });
        });
    }

    this.startSliders = function(){
        var objThis     = this;
        var tpl         = '<fieldset class="slider{id}Wrap"><legend>{leyenda}</legend><input type="hidden" class="{clase}" id="{id}" value="{value}" /><input type="number" id="{id}Control" class="measure" value="{value}" min="{min}" max="{max}" step="{step}"></fieldset>';

        $('.measureGroup').html('');
        Object.keys(config.slidersFields).forEach(function loop(key){
            var html = tpl;
            html = html.replace(/{id}/g,        key);
            html = html.replace(/{leyenda}/g,   objThis.getTranslate(config.slidersFields[key].legend));
            html = html.replace(/{clase}/g,     config.slidersFields[key].class);
            html = html.replace(/{value}/g,     parseFloat(config.slidersFields[key].value));
            html = html.replace(/{min}/g,       parseFloat(config.slidersFields[key].min));
            html = html.replace(/{max}/g,       parseFloat(config.slidersFields[key].max));
            html = html.replace(/{step}/g,      config.slidersFields[key].step);
            $('.measureGroup').append(html);
        });

        $('#zoomScale').jRange(config.zoom);
        $('#width').jRange(config.sliders);
        $('#height').jRange(config.sliders);
        config.sliders.from = 0;
        $('#startX').jRange(config.sliders);
        $('#startY').jRange(config.sliders);  
    }

    /* This function is called when scale slider change */
    this.setScale 		= function(){
        var scale = $('#zoomScale').val();
        if(scale != 0){
            $('.surface').css('zoom',scale/100).css('zoom',scale+"%").css('-ms-zoom',scale+"%");
            this.zoom = scale;
            $('.wrap').css('left',0).css('top',0);
            this.updateSurface();
        }
    }

    /* 
    This function is called when measure button (#toggleMeasure) is clicked on simulator header
    It is used to show or hide grid and dimensions on surface
    */
    this.toggleMeasure = function(){
        if(this.grid){
            $('.wrapSimulator').removeClass('showMeasure');
            this.grid = false;
        } else {
            $('.wrapSimulator').addClass('showMeasure');
            this.grid = true;
        }
    }

    this.changeSurfaceMeasure = function(){
        var vals = {
            'width':    $('#width').val(),
            'height':   $('#height').val(),
            'X':        $('#startX').val(),
            'Y':        $('#startY').val(),
        }

        var surfObj            = this.surface[this.currentSurface];
    	surfObj.width          = vals.width;
    	surfObj.height         = vals.height;
    	surfObj.x              = vals.X;
    	surfObj.y              = vals.Y;
    	this.updateSurface();
        this.checkLineFitSurface();
        this.updateAdjoiningWalls();
    }

    this.GetLimits = function(){
        return limits = {
            'sceneW':       $("#simulator").outerWidth(true),
            'sceneH':       $("#simulator").outerHeight(true),
            'surfaceW':     $('.wrap').outerWidth(true),
            'surfaceH':     $('.wrap').outerHeight(true),
        }
    }

    this.returnConnectionArray = function(values){
        var connect         = values.split('');
        var cont            = 1;
        var result          = {}
        Object.keys(connect).forEach(function(key) {
            result[cont] = connect[key];
            cont++;
        });
        return result;
    }

    this.centerSurfaceFnc = function(){
        if( $('.surface.active').length ){
            var limits          = this.GetLimits(); 
            limits.surfaceW     = parseInt($('.surface.active').attr('data-width'));
            limits.surfaceH     = parseInt($('.surface.active').attr('data-height')) + 50;
            var scaleW          = Math.floor(limits.sceneW/limits.surfaceW * 88) ;
            var scaleH          = Math.floor(limits.sceneH/limits.surfaceH * 88) ;
            var scale           = scaleW <= scaleH ? scaleW : scaleH;
            scale               = scale < parseInt(startValues.zoom.from) ? parseInt(startValues.zoom.from) : scale;
            this.zoom           = scale;
            var sendData        = {
                'targetItem':   'zoomScale',
                'minValue':     startValues.zoom.from,
                'maxValue':     startValues.zoom.to,
                'current':      scale.toString(),
            }
            this.changeSliderValue(sendData);
        }
    }

    this.updateSurface = function(id){
        if (typeof(id) == "undefined"){
            var id      = this.currentSurface;
        }
        var sObj    = this.surface[id];
        var obj     = this;
        var jqObj   = $('#'+id);

        if (typeof(sObj) != "undefined"){
            jqObj.width(
                this.mmToPx(sObj.width)
            ).height(
                this.mmToPx(sObj.height)
            ).attr(
                'data-width',this.mmToPx(sObj.width)
            ).attr(
                'data-height',this.mmToPx(sObj.height)
            );

            var ancho   = this.mmToPx(sObj.x);
            var alto    = this.mmToPx(sObj.y);
            this.currentX = ancho;
            this.currentY = alto;

            $('.wrap').css('margin',(obj.zoom*0.3)+"px"); 

            jqObj.find('.hmeasure').width(ancho);
            if(sObj.x <= 400){
                jqObj.find('.hmeasure').addClass('mini');
            } else {
                jqObj.find('.hmeasure').removeClass('mini');
            }
            jqObj.find('.hmeasure span').html(sObj.x + this.unit);

            jqObj.find('.vmeasure').width(alto).css('left',((alto * -1) - 28) + "px");
            console.log((alto+18)*(-1) + "px");
            if(sObj.y <= 400){
                jqObj.find('.vmeasure').addClass('mini');
            } else {
                jqObj.find('.vmeasure').removeClass('mini');
            }
            jqObj.find('.vmeasure span').html(sObj.y + this.unit);

            jqObj.find('.guide').width(ancho-1).height(alto-1);


            var currentLine = sObj.currentLine;
            $('#'+currentLine).css('top',alto + "px").css('left',ancho + "px");

            this.commonUpdatings();
        }
    }

    this.updateMaxMeasure = function(){
        var objCurrentSurface   = this.surface[this.currentSurface];
        var cLineID             = objCurrentSurface.currentLine;
        var currentLine         = objCurrentSurface.lines[cLineID];
        var lx                  = $('#startX').val();
        var ly                  = $('#startY').val();
        var starJoin            = typeof(currentLine) != "undefined" ? currentLine.starJoin : false;
        if (objCurrentSurface.width <= parseFloat(lx) || starJoin == 2){
            var dataSet = objCurrentSurface.width.toString();
            $('#startX').jRange('updateRange', 0+','+dataSet , dataSet);
            $('#startXControl').val(dataSet);
        } else {
            $('#startX').jRange('updateRange', 0+','+objCurrentSurface.width.toString(), lx);  
        }
        $('#startXControl').attr('max',objCurrentSurface.width.toString());

        if (objCurrentSurface.height <= parseFloat(ly) || starJoin == 3){
            var dataSet = objCurrentSurface.height.toString();
            $('#startY').jRange('updateRange', 0+','+dataSet , dataSet);
            $('#startYControl').val(dataSet);
        } else {
            $('#startY').jRange('updateRange', 0+','+objCurrentSurface.height.toString(), ly);  
        }
        $('#startYControl').attr('max',objCurrentSurface.height.toString());
    }

    this.setSliderValues = function(obj){
        var values = {
            'targetItem': obj.closest('fieldset').find('.mSlider').attr('id'),
            'minValue': obj.attr('min'),
            'maxValue': obj.attr('max'),
            'current': obj.val()
        }
        this.changeSliderValue(values);
    }

    this.changeSliderValue = function(obj){
        $('#' + obj.targetItem).jRange('updateRange', obj.minValue+','+obj.maxValue , obj.current);
        if(obj.targetItem != 'zoomScale'){
            this.changeSurfaceMeasure();
        }
    }

    /* 
    This function is called after updating the surface measurements
    It is used to check if the new measurement can be dragged into the view
    showing, hiding or activating the movement arrows when it's necessary
    */
    this.checkDraggable = function(){
        var jqObj   = $('#'+this.currentSurface);
        var limits = this.GetLimits();

        var X1 = (limits.sceneW +  $("#simulator").offset().left - limits.surfaceW);
        var Y1 = (limits.sceneH +  $("#simulator").offset().top - limits.surfaceH) - 50;
        var X2 = $("#simulator").offset().left + 3;
        var Y2 = $("#simulator").offset().top + 3;

        if(limits.sceneW < limits.surfaceW || limits.sceneH < limits.surfaceH){
            $(".wrapSimulator").addClass('draggable');
            var objeto = this;
            obj = {
                containment:[X1, Y1, X2, Y2],
                scroll:     false,
                cursor:     "all-scroll",
                drag:       function( event, ui ) {
                    objeto.updatePosi({
                        'left': parseFloat($('.wrap').css('left')), 
                        'top': parseFloat($('.wrap').css('top'))
                    });
                }
            }
            $( ".draggable .wrap" ).draggable(obj);

            $('#dragTop, #dragBottom, #dragRight, #dragLeft').show();
            if(limits.surfaceW <= limits.sceneW){
                $( ".draggable .wrap" ).draggable( "option", "axis", "y" );
                $('#dragRight, #dragLeft').hide();
            } else if(limits.surfaceH  <= limits.sceneH - 50){
                $( ".draggable .wrap" ).draggable( "option", "axis", "x" );
                $('#dragTop, #dragBottom').hide();   
            } else{
                $( ".draggable .wrap" ).draggable( "option", "axis", false );
            }
            this.moveSurface('update');
        } else {
            $( ".draggable .wrap" ).draggable( "destroy" );
            $(".wrapSimulator").removeClass('draggable');
        }

        var leftPosi        = limits.sceneW > limits.surfaceW ? parseInt((limits.sceneW - limits.surfaceW)/2) : 0;
        var topPosi         = limits.sceneH > limits.surfaceH ? parseInt((limits.sceneH - limits.surfaceH)/2) - 25 : 0;
        topPosi             = topPosi < 0 ? 0 : topPosi + 'px';
        leftPosi            = leftPosi < 0 ? 0 : leftPosi + 'px';

        $('.wrap').css('top',topPosi).css('left',leftPosi);
        this.updatePosi( {'left':leftPosi,'top':topPosi} );
        this.updateMaxMeasure();
    }

    this.updatePosi = function(posi){
        var obj     = this.surface[this.currentSurface];
        obj.posiX   = posi.left;
        obj.posiY   = posi.top;
        this.moveSurface('update');
    }

    this.moveSurface = function(type){
        var simObj         = {
            'maxX': 0,
            'maxY': 0,
            'minX': ($('.wrap').outerWidth(true) - $("#simulator").outerWidth(true)) * (-1),
            'minY': ($('.wrap').outerHeight(true) - $("#simulator").outerHeight(true)) * (-1) - 50,
            'step': 10,
        }
        if(simObj.minX > 0){
            simObj.minX = 0;
        }
        if(simObj.minY > 0){
            simObj.minY = 0;
        }

        var targetSurface      = this.surface[this.currentSurface];

        switch(type){
            case 'top':
                currentY = targetSurface.posiY + simObj.step;
                currentX = targetSurface.posiX;
            break;

            case 'right':
                currentX = targetSurface.posiX - simObj.step;
                currentY = targetSurface.posiY;
            break;

            case 'bottom':
                currentY = targetSurface.posiY - simObj.step;
                currentX = targetSurface.posiX;
            break;

            case 'left':
                currentX = targetSurface.posiX + simObj.step;
                currentY = targetSurface.posiY;
            break;

            case 'update':
                currentX = targetSurface.posiX;
                currentY = targetSurface.posiY;
            break;
        }

        $('.navMove a').removeClass('disable');
        if(currentY >= 0){ currentY = 0; $('#dragTop').addClass('disable') }
        if(currentY <= simObj.minY){ currentY = simObj.minY; $('#dragBottom').addClass('disable') }
        if(currentX >= 0){ currentX = 0; $('#dragLeft').addClass('disable') }
        if(currentX <= simObj.minX){ currentX = simObj.minX; $('#dragRight').addClass('disable') }

        $('.wrap').css('top',currentY).css('left',currentX);
        targetSurface.posiX = currentX;
        targetSurface.posiY = currentY;
    }

    this.disableSliders = function(targetItem,surfaceID){
        var dataItem = {
            'id'        :'',
            'value'     :'',
        }
        switch(targetItem){
            case 's1':
                dataItem.id     = $('#startYControl');
                minV            = dataItem.id.attr('min').toString();
                maxV            = this.surface[surfaceID].height.toString();
                currV           = minV.toString();
            break;
            case 's2':
                dataItem.id     = $('#startXControl');
                minV            = dataItem.id.attr('min').toString();
                maxV            = this.surface[surfaceID].width.toString();
                currV           = maxV.toString();
            break;
            case 's3':
                dataItem.id     = $('#startYControl');
                minV            = dataItem.id.attr('min').toString();
                maxV            = this.surface[surfaceID].height.toString();
                currV           = maxV.toString();
            break;
            case 's4':
                dataItem.id     = $('#startXControl');
                minV            = dataItem.id.attr('min').toString();
                maxV            = this.surface[surfaceID].width.toString();
                currV           = minV.toString();
            break;
        }
        if( surfaceID == this.currentSurface && dataItem.id != ''){
            var obj     = dataItem.id;
            var wrap    = obj.closest('fieldset');
            var slider  = wrap.find('.mSlider').attr('id');
            $('#'+slider).jRange('updateRange', minV+','+maxV , currV); 
            $('#'+slider).jRange('disable');
            obj.attr('disabled','disabled');
            wrap.addClass('disabled');
        }
    }

    this.enableSliders = function(){
        var currentLine             = this.getCurrentLine();
        var totalItemsInLine        = Object.keys(currentLine.items).length;

        $('#startY').jRange('enable');
        $('#startX').jRange('enable');
        $('fieldset.disabled').removeClass('disabled');
        $('.measure').removeAttr('disabled');
        if( totalItemsInLine > 0){
            var firstItemID = Object.keys(currentLine.items)[0];
            this.disableSliders(currentLine.items[firstItemID].type,this.currentSurface);
        }
    }

    this.updateItemList = function(){
        var currentLine             = this.getCurrentLine();
        var items                   = currentLine.items;
        var total                   = Object.keys(items).length;
        var cont                    = 1;
        var removeText              = this.surface[this.currentSurface].currentLine + "_";
        $('#itemList option.item').remove();

        Object.keys(items).forEach(function loop(key){
            var text = key.replace(removeText,'');
            obj = {
                value: key,
                text : text + " - " + items[key].type,
                class: 'item',
            };
            if(cont == total){
                obj['selected'] = 'selected';
            }
            $('#itemList').append($('<option>',obj));
            cont++;
        });
        this.selectItem();
    }

    this.updateNextPosition = function(){
        var currentLine     = this.getCurrentLine();
        var Surface         = $('#' + this.currentSurface);
        Surface.find('.addProd').css('top',currentLine.nextY).css('left',currentLine.nextX);
        this.setLineDimentions(currentLine.items);
        this.toggleDirections();
        this.updateItemList();
    }

    this.getTotalStraight = function(){
        var currentLine             = this.getCurrentLine();
        var items                   = currentLine.items;
        var total                   = Object.keys(items).length;
        var rectas                  = this.rectasArray;
        var obj                     = this;
        var type                    = '';
        var polarity                = 'A';
        if(total>1){
            Object.keys(items).forEach(function loop(key){
                type                = items[key].type;
                polarity            = items[key].polarity;
                if(rectas.includes(type)){
                    large           = obj.mmToPx( parseFloat( type.substring(1,type.length) ) );
                    if( type.substring(0,1) == currentLine.rectas.type){
                        currentLine.rectas.items.push(large);
                    } else {
                        currentLine.rectas.type         == type.substring(0,1);
                        currentLine.rectas.items        = [];
                        currentLine.rectas.items.push(large);   
                    }
                } else {
                    currentLine.rectas.items      = [];
                }
            });
        } else if(total == 1){
            type                = items[ Object.keys(items) ].type;
            polarity            = items[ Object.keys(items) ].polarity;
        }
        currentLine.lastItem = type;
        currentLine.polarity = polarity;
    }

    this.reduceStraight = function(rectas){
        var totalItems = rectas.items.length;
        // Entramos si hay más de dos rectas seguidas para comprobar si podemos
        // reemplazar los items por rectas más grandes
 
        if( totalItems > 1){
            var sizes           = this.rectasSizes;
            var total           = this.sumTotalStraight(rectas);
            var newLarge        = 0;
            var posi            = 0;
            var stopLoop        = false; 
            var objThis         = this;

            Object.keys(rectas.items).forEach(function loop(key){
                if(loop.stop){ return; }
                if(Math.floor(total) in sizes){
                    loop.stop   = true; 
                    newLarge    = total;
                    posi        = key;
                    stopLoop    =  true;
                } else {
                    total       = total - parseFloat(rectas.items[key]);
                    total       = objThis.processMeasureStraight( rectas.type, parseFloat(total.toFixed(1)) , 1 );
                }
            });
            if(stopLoop && (totalItems - 1) != posi){
                objThis.deleteItems( (totalItems - posi));
                objThis.addItem(sizes[Math.floor(total)],false,false,objThis.currentSurface);
            }
        }
    }

    this.selectItem = function(){
        var Surface         = $('#' + this.currentSurface);
        var id              = $("#itemList").val();
        Surface.find('.prodItem').removeClass('deletable').removeClass('active');
        if(id != 0){
            $('.wrapSimulator').addClass('editItem');
            $('#'+id).addClass('active');
            var min = Surface.find('.prodItem.active').attr('data-posi');
            Surface.find(".prodItem:gt("+min+")" ).addClass('deletable');
        } else {
            $('.wrapSimulator').removeClass('editItem');
        }
    }

    this.countDelectableItems = function(){
        var currentLine     = this.getCurrentLine();
        var items           = currentLine.items;
        var total           = Object.keys(items).length;
        var Surface         = $('#' + this.currentSurface);
        var min             = Surface.find('.prodItem.active').attr('data-posi');
        if (typeof(min) != "undefined"){
            this.deleteItems(total - min);
            this.updateNextPosition();
        }
    }

    this.deleteItems = function(count){
        var currentLine             = this.getCurrentLine();
        for(x=0;x<count;x++){
            last                    = Object.keys(currentLine.items)[Object.keys(currentLine.items).length-1];
            lastR                   = Object.keys(currentLine.rectas.items)[Object.keys(currentLine.rectas.items).length-1];

            $('#'+last).remove();
            delete currentLine.items[last];
            currentLine.rectas.items.splice(-1);
        }
        if( Object.keys(currentLine.items).length == 0){
            currentLine.nextX       = 0;
            currentLine.nextY       = 0;
            currentLine.starJoin    = currentLine.endJoin = false;      
        } else {
            last                    = Object.keys(currentLine.items)[Object.keys(currentLine.items).length-1];
            obj                     = currentLine.items[last];
            currentLine.nextX       = obj.nextX;
            currentLine.nextY       = obj.nextY;
            currentLine.startPosi   = obj.nP;
            currentLine.direction   = obj.nD;
        }
        this.deletingFunctions();
    }

    this.deletingFunctions = function(){
        this.enableSliders();
        this.getTotalStraight();
        this.updateNextPosition();  
    }

    this.commonUpdatings = function(){
        this.checkDraggable();
    }

    this.sumTotalStraight = function(rectas){
        var thisObj = this;
        var temp = total = 0;
        Object.keys(rectas.items).forEach(function loop(key){
            temp    = parseFloat(temp) + thisObj.processMeasureStraight(rectas.type, parseFloat(rectas.items[key]),-1);
            total   = temp;//.toFixed(1);
        });
        return total;
    }

    /* This function is called each time than installation change (adding or removing element) */
    this.setLineDimentions = function(items){
        var limits = {
            'minX': 0,
            'maxX': 0,
            'minY': 0,
            'maxY': 0,
        }
        var units = {};
        
        Object.keys(items).forEach(function(key) {
            if(items[key].posiX < 0){
                limits.minX = limits.minX > parseFloat(items[key].posiX) ? parseFloat(items[key].posiX) : limits.minX;
            }
            var maxX = parseFloat(items[key].width) + parseFloat(items[key].posiX);
            limits.maxX = limits.maxX < maxX ? maxX : limits.maxX;

            if(items[key].posiY < 0){
                limits.minY = limits.minY > parseFloat(items[key].posiY) ? parseFloat(items[key].posiY) : limits.minY;
            }
            var maxY = parseFloat(items[key].height) + parseFloat(items[key].posiY);
            limits.maxY = limits.maxY < maxY ? maxY : limits.maxY;

            unitsShow = {
                'x':    items[key].posiX,
                'y':    items[key].posiY,
                'w':    items[key].width,
                'h':    items[key].height,
                'newx': maxX,
                'newy': maxY,
                'minX': limits.minX,
                'maxX': limits.maxX,
                'minY': limits.minY,
                'maxY': limits.maxY,
            }
        });
        this.updateGroupSize(limits);
        this.updateMaxMeasure();
    }

    /* updating div group sizes after setLineDimentions function is called */
    this.updateGroupSize = function(limits){
        var currentLine         = this.getCurrentLine();
        var width               = limits.maxX + limits.minX * (-1);
        var height              = limits.maxY + limits.minY * (-1);
        var widthMm             = this.pixToMm(width);
        var heightMm            = this.pixToMm(height);
        var Surface             = $('#' + this.currentSurface);

        Surface.find('.group').show();
        Surface.find('.group').css('width',width + "px").css('height',height + "px");
        Surface.find('.group').attr('data-width',widthMm + this.unit);
        Surface.find('.group').attr('data-number',widthMm);
        Surface.find('.group .measureY').attr('data-height',heightMm + this.unit);

        if(heightMm == 0 || !$('.wrapSimulator').hasClass('showMeasure')){
            Surface.find('.group .measureY').hide();
        } else {
            Surface.find('.group .measureY').show();
        }

        if(widthMm == 0){
            Surface.find('.group').before().hide();
        } else {
            Surface.find('.group').before().show();
        }

        if(widthMm <= 200){
            Surface.find('.group').addClass('mini');
        } else {
            Surface.find('.group').removeClass('mini');            
        }

        currentLine.width   = widthMm;
        currentLine.height  = heightMm;
        currentLine.x       = this.pixToMm(limits.minX);
        currentLine.y       = this.pixToMm(limits.minY);

        Surface.find('.group').css('left',limits.minX + "px").css('top',limits.minY + "px");
        this.checkLineFitSurface();
    }

    /* function to convert pixels to millimeters */
    this.pixToMm = function(number){
        return Math.floor(number * this.scaleMm);
    }

    /* function to convert millimeters to pixels */
    this.mmToPx = function(number){
        number      = number * this.scalePix;
        number      = number.toFixed(1);
        return number;
    }

    /* 
    This function is called every time it have changes the installation, view or surface 
    add or remove elements, change the size of the surface, change the view scale, etc.)
    It check that the installation fits on the surface or not
    */
    this.checkLineFitSurface = function(){
        var currentLine     = this.getCurrentLine();
        var surface         = this.surface[this.currentSurface];
        var left            = parseFloat(surface.x) + parseFloat(currentLine.x);
        var top             = parseFloat(surface.y) + parseFloat(currentLine.y);
        var width           = parseFloat(surface.x) + parseFloat(currentLine.width) + parseFloat(currentLine.x);
        var height          = parseFloat(surface.y) + parseFloat(currentLine.height) + parseFloat(currentLine.y);
        if( left < 0 || top < 0 || width > surface.width || height > surface.height){
            $('.wrapSimulator').addClass('fixError');
        } else {
            $('.wrapSimulator').removeClass('fixError');
        }
    }

    /* 
    This function is called when clicking on direction arrows,
    It only available if the surface current line its empty
    */
    this.newDirection = function(current){
        var currentLine         = this.getCurrentLine();
        currentLine.direction   = current.attr('data-direction');
        currentLine.startPosi   = current.attr('data-start');
        $('.directions a').removeClass('active');
        current.addClass('active');
        this.toggleComponents();
    }

    /* 
    This function is called whenever elements are added or deleted
    It is used to calculate the following direction and shows the available components for it
    */
    this.toggleDirections = function(){
        var currentLine         = this.getCurrentLine();
        if (Object.keys(currentLine.items).length > 0){
            $('.directions').addClass('disabled');
        } else {
            $('.directions').removeClass('disabled');
        }
        $('.directions a').removeClass('active');
        $('.directions a[data-start='+currentLine.startPosi+']').addClass('active');
        this.toggleComponents();
    }

    /* This function is used to get the current line active */
    this.getCurrentLine = function(){
        var cLineID             = this.surface[this.currentSurface].currentLine;
        var currentLine         = this.surface[this.currentSurface].lines[cLineID];
        return currentLine;  
    }

    this.changePage = function(page){
        this.page = page;
            if(this.page != 'page_selectTemperature'){
            $('.page.pageActive').removeClass('pageActive');
            $('#'+page).addClass('pageActive');
            this.browserAdjust(this);
        } else {
            alert('En desarrollo');
        }
    }

    this.getLarge = function(number,large){
        var number                   = number.toString();
        return parseFloat(number.replace('L',large));
    }

    this.browserAdjust = function(ObjThis){
        $('.autoHeight, .content').css('min-height','auto');
        var less        = $(window).height();
        less            = $('.content').outerHeight() > less ? $('.content').outerHeight() : less;

        $('.autoHeight, .content').css('min-height',less);
        if (typeof ObjThis.updateSurface == 'function') {
            ObjThis.updateSurface();
        }
        if (typeof ObjThis.centerSurfaceFnc == 'function') {
            ObjThis.centerSurfaceFnc();  
        }
    }

    this.addSurface         = function(type,connect){
        id                  = this.surfaceTypes[type].id;
        label               = this.surfaceTypes[type].name;
        var setMeasureUnits = {
            'w': 'surfaceWidth',
            'h': 'surfaceHeight',
            'd': 'surfaceDepth',
        }

        var width           = setMeasureUnits[ this.surfaceTypes[type].measure['w'] ];
        var height          = setMeasureUnits[ this.surfaceTypes[type].measure['h'] ];

        var thisConfigObj   = {
            'width':        startValues[width],
            'height':       startValues[height],
            'measureW':     this.surfaceTypes[type].measure['w'],
            'measureH':     this.surfaceTypes[type].measure['h'],
            'x':            startValues.slidersStartX,
            'y':            startValues.slidersStartY,
            'lines':        [],
            'currentLine':  '',
            'nextPosiX':    0,
            'nextPosiY':    0,
            'posiX':        0,
            'posiY':        0,
            'connect':      connect,
            'type':         type,
            'connectedTo':  false
        }

        this.surface[id] = thisConfigObj;

        $('#simulator .wrap').append('<div id="'+id+'" class="surface" data-width="'+thisConfigObj.width+'" data-height="'+thisConfigObj.height+'" data-label="'+label+'"></div>');
        $('#'+id).append('<div class="legend hmeasure"><span>'+thisConfigObj.x+this.unit+'</span></div><div class="legend vmeasure"><span>'+thisConfigObj.y+this.unit+'</span></div><div class="guide"></div>');      
        if( !this.currentSurface ){
            this.currentSurface = id;
            $('#'+id).addClass('active');
        } 
        this.updateSurface(id);
        this.addLine(id);
    };

    this.addLine = function(id){
        var objS        = this;
        var lines       = this.surface[id].lines;
        if(Object.keys(lines).length == 0){
            var idLine = id + '_line_01';
            objS.surface[id].currentLine = idLine;
            $('#'+id).append('<div id="'+idLine+'" class="line"><a href="#" class="addProd">More</a><div class="group"><div class="measureY"></div></div></div>');
            $('#'+idLine).css('top',objS.currentY + "px").css('left',objS.currentX + "px");
            obj = {
                'nextX':        0,
                'nextY':        0,
                'items':        [],
                'direction':    'h',
                'startPosi':    4,
                'rectas':       {'type':'','items':[]},
                'starJoin':     false,
                'endJoin':      false,
                'polarity':     'A',
                'lastItem':     '',
                'x':            0,
                'y':            0
            }
            lines[idLine] = obj;
            objS.toggleComponents();
        }
    }

    this.addItem = function(type,reduce,linkObj,SurfaceID){

        var IObj                = {};
        IObj.thisSurface        = this.surface[SurfaceID];
        IObj.cLineID            = IObj.thisSurface.currentLine;
        IObj.currentLine        = IObj.thisSurface.lines[IObj.cLineID];
        IObj.items              = IObj.currentLine.items;
        IObj.sP                 = IObj.currentLine.startPosi; 
        IObj.dir                = IObj.currentLine.direction;
        IObj.total              = Object.keys(IObj.items).length;
        IObj.large              = 0;
        IObj.itemID             = 'M' + (IObj.total + 1);
        IObj.objectID           = IObj.cLineID + "_" + IObj.itemID;
        IObj.posi               = IObj.total;
        IObj.changePolarity     = false;
        IObj.canAdd             = true;
        IObj.type               = type;
        IObj.SurfaceID          = SurfaceID;
        IObj.linkObj            = linkObj;
        IObj.reduce             = reduce;
        IObj.clase              = IObj.currentLine.direction == 'v' ? ' vertical' : ''; 
        IObj.obj                = {
            'type':                 type
        } 

        IObj = this.addItemByLine(IObj);

        if(IObj.canAdd){
            $('#'+IObj.cLineID).append(IObj.html);
            IObj.obj.width   = this.getLarge(IObj.data.width,IObj.large);
            IObj.obj.height  = this.getLarge(IObj.data.height,IObj.large);
            IObj.obj.order   = IObj.total;
            IObj.obj.posiX   = IObj.stPos.x;
            IObj.obj.posiY   = IObj.stPos.y;
            IObj.obj.nextX   = IObj.X;
            IObj.obj.nextY   = IObj.Y;
            IObj.obj.nP      = IObj.data.next;
            IObj.obj.nD      = IObj.data.dir;

            IObj.currentLine.direction   = IObj.data.dir;
            IObj.currentLine.startPosi   = IObj.data.next;
            IObj.currentLine.nextX       = IObj.X;
            IObj.currentLine.nextY       = IObj.Y;
            IObj.currentLine.lastItem    = IObj.obj.type;

            IObj.items[IObj.objectID] = IObj.obj;
            this.updateNextPosition();

            if(IObj.reduce){
                this.reduceStraight(IObj.currentLine.rectas);
            }
            if(IObj.addOpposite){
                var SendData = {
                    'surface':      IObj.thisSurface,
                    'connect':      IObj.connectTo,
                    'dir':          IObj.data.dir
                }
                this.getOppositeLineStartData(SendData,true);
            }
        }
    }

    this.getTranslate = function(tag){
        return typeof(this.translation[tag]) != "undefined" ? this.translation[tag] : '['+tag+']' ;
    }

    this.traduceTemplate = function(){
        var objThis         = this;
        $('[data-tr]').each(function(){
            var currentTag = $(this).attr('data-tr');
            var text = objThis.getTranslate(currentTag);
            $(this).html(text);
            if($(this).is('a[href]')){
                $(this).attr('title',text);
            }
        });
    }

    this.showModal = function(text,type){
        var btnText = this.getTranslate('btnCloseText');
        $('.contentModal').append('<p>'+text+'</p><a href="#" class="btn btnCloseModal">' + btnText + '</a>');
        $('#modal').fadeIn(400);
    }

    this.hideModal = function(){
        $('#modal').fadeOut(400,function(){
            $('.contentModal').html('');
        });
    }
}