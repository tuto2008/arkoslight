<?php
class routes{
    private $folder                     = '';
    private $baseUrl                    = '';
    private $path;
    private $url;
    private $tpl;
    private $params                     = array(
        'family'            => 'home',
        'child'             => false,
    );
    private $valid 						= array('blackfoster','brightline','fifty');
    private $fifty                      = array('wall','trimless','suspension','surface','recessed');

    public function __construct($tpl){
        $this->setEnviroment();
        $this->tpl                      = $tpl;
        $this->url                      = explode( "/",$this->path );
        $this->baseUrl                  = "http://".$_SERVER["SERVER_NAME"].$this->folder;
    	$this->processUrl();
    }

    private function setEnviroment(){
        // die($_SERVER['DOCUMENT_ROOT']);

        //si se ejecuta direactemente en el root, si folder
        switch ($_SERVER['DOCUMENT_ROOT']) {
            case '/Applications/MAMP/htdocs/arkoslight': //local Luis
            case 'C:/wamp64/www/arkos': //local Omar
                $this->folder           = '/';
                $this->path             = substr(strtolower( $_SERVER["REQUEST_URI"] ),1);
            break;
        }

        //si se ejecuta en algun folder del localhost o dominio local
        /*
        case 'tu ruta local':
        $this->folder           = '/deploy/'; //tu folder local a partir del dominio o localhost
        $this->path             = strtolower( str_replace($this->folder,"", $_SERVER["REQUEST_URI"]) );
        break;
        */
    }

	// procesamos los parámetros de la url (friendly Urls)
    private function processUrl(){
        //Its a Family
        if (in_array($this->url[0] , $this->valid)){
            $this->params['family'] = $this->url[0];

            if($this->url[0] == 'fifty'){
                //Its a child of fifty
                if (isset($this->url[1]) && in_array($this->url[1] , $this->fifty)){
                    $this->params['child'] = $this->url[1];
                }
            }
        }
    }

    public function startTpl(){
        switch($this->params['family']){
            case 'home':
                $this->tpl->newBlock("home_menu");
            break;

            case 'brightline':
                $this->tpl->newBlock("family_brightline");
            break;

            case 'blackfoster':
                $this->tpl->newBlock("family_blackfoster");
            break;

            case 'fifty':
                $this->tpl->newBlock("family_fifty");
                if( !$this->params['child'] ){
                   $this->tpl->newBlock("fifty_select");
                } else {
                   $this->tpl->newBlock("fifty_childs");
                   $this->tpl->assign("Fifty_item",             'Fifty '.strtoupper($this->params['child']));
                }
            break;
        }
    }

    public function printToScreen(){
        $this->tpl->printToScreen();
    }

    public function getBody(){
        //TODO change body class
        $class = $this->params['child'] ? ' class="family-'.$this->params['child'].'" data-family="'.$this->params['child'].'"' : '';
        return 'id="'.$this->params['family'].'"'.$class;
    }

    public function getUrl(){
        return $this->baseUrl;
    }
}
?>