<?php 
	$ww 		= 800;
	$hh 		= 600;
	$w 			= 800;
	$h 			= 600;
	$l 			= 1200;
	$a 			= (($l/2) + ($w-$l));
	$b 			= (($l/2) + ($w-$l)) * (-1);
	$factor 	= 1.1 + (intval(($l - $w)/100) * 0.13);
	$x 			= $l <= $w ? $l/2 + (($w - $l)/2) : (($w/2) - (($l - $w)*$factor) - 85);
	$xx 		= $x * (-1);
	$pers 		= 900;

	$zoom 		= 100;

	if($w > $ww){
		$zoom 		= ($ww*100)/$w;
	} else if($h > $hh){
		$zoom 		= ($hh*100)/$h;
	}
?>
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Document</title>
	<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.8.3/jquery.min.js"></script>
	<style>
		*{box-sizing:border-box}
		body{font-family:sans-serif}
		.wrap{width:<?php echo $ww + 4; ?>px;height:<?php echo $hh + 4; ?>px;border:3px solid #CCC;overflow:hidden;margin:80px;background:#eaeaea;}
		#contentCube{width:<?php echo $w; ?>px;height:<?php echo $h; ?>px;perspective:<?php echo $pers; ?>px;position:relative;top:50%;left:50%;margin-top:-<?php echo $h/2; ?>px;margin-left:-<?php echo $w/2; ?>px;zoom:<?php echo $zoom."%"; ?>;-webkit-transition:zoom 0.5s linear;-moz-transition:zoom 0.5s linear;-o-transition:zoom 0.5s linear;transition:zoom 0.5s linear}
		#cube{width:<?php echo $w; ?>px;height:<?php echo $h; ?>px;position:relative;transform-style:preserve-3d;transform:translateZ(-<?php echo $l; ?>px);transition:transform 1s;top:-1px;left:-1px;zoom:100%;}

		.show-none #cube{transform:translateZ(-<?php echo $l; ?>px) rotateY(0deg)} 
		.show-izq #cube{transform:rotateY(-90deg) translateZ(-<?php echo $l/2; ?>px) translateX(<?php echo $x; ?>px)}
		.show-back #cube{transform:translateZ(-85px) rotateY(0deg)}
		.show-der #cube{transform:rotateY(90deg) translateZ(-<?php echo $l/2; ?>px) translateX(<?php echo $xx; ?>px)}
		.show-top #cube{transform:rotateX(-90deg)}
		.show-ceiling #cube{transform:translateZ(<?php echo $h - $l; ?>px) translateY(<?php echo $l/2; ?>px) rotateX(90deg)}

		.surface{position:absolute;width:<?php echo $w; ?>px;height:<?php echo $h; ?>px;border:4px solid #c7c7c7;-webkit-transition:opacity 0.5s linear;-moz-transition:opacity 0.5s linear;-o-transition:opacity 0.5s linear;transition:opacity 0.5s linear}
		.surface::before{content:attr(data-label);font-size:12px;text-transform:uppercase;position:absolute;left:20px;bottom:20px;float:left;display:inline;line-height:normal;color:#989898}
		.faceizq::before{left:auto;right:20px}
		.faceback::before, .facetop::before{left:0;right:0;width:100%;text-align:center}
		.facetop::before{bottom:auto;top:20px}

		.faceizq{transform:rotateY(-90deg) translateZ(<?php echo $b ?>px) translateX(<?php echo $l/2; ?>px);width:<?php echo $l; ?>px;height:<?php echo $h; ?>px}
		.faceback{transform:rotateY(0) translateZ(0)}
		.faceder{transform:rotateY(90deg) translateZ(-<?php echo $l/2; ?>px) translateX(-<?php echo $l/2; ?>px);width:<?php echo $l; ?>px;height:<?php echo $h; ?>px}
		.facetop{transform:rotateX(-90deg) translateZ(-<?php echo $l/2; ?>px) translateY(-<?php echo $l/2; ?>px);height:<?php echo $l; ?>px;width:<?php echo $w; ?>px}
		.facefloor{transform:rotateX(-90deg) translateZ(<?php echo $a ?>px) translateY(-<?php echo $l/2; ?>px);height:<?php echo $l; ?>px;width:<?php echo $w; ?>px}

		.show-izq #cube .faceizq, .show-der #cube .faceder{opacity:0}
		.show-izq #cube .facetop, .show-der #cube .facetop, .show-izq #cube .facefloor, .show-der #cube .facefloor{border-color:transparent}
		.show-back #cube .faceizq, .show-back #cube .faceder{opacity:1}
		.show-none #cube .facetop, .show-none #cube .facefloor{border-color:transparent}

		label{margin-right:10px}
	</style>
</head>
<body>
	<p class="radio-group">
	  <label>
	    <input type="radio" name="rotate-cube-side" value="none" checked /> Todos
	  </label>
	  <label>
	    <input type="radio" name="rotate-cube-side" value="izq" /> Izquierda
	  </label>
	  <label>
	    <input type="radio" name="rotate-cube-side" value="back" /> Fondo
	  </label>
	  <label>
	    <input type="radio" name="rotate-cube-side" value="der" /> Derecha
	  </label>
	  <label>
	    <input type="radio" name="rotate-cube-side" value="ceiling" /> Techo
	  </label>
	</p>
	<div class="wrap">
		<div id="contentCube">
		  <div id="cube">
		    <div class="surface faceback" data-label="Pared Fondo"></div>
		    <div class="surface faceizq" data-label="Pared Derecha"></div>
		    <div class="surface faceder" data-label="Pared Izquierda"></div>
		    <div class="surface facetop" data-label="Techo"></div>
		    <div class="surface facefloor" data-label=""></div>
		  </div>
		</div>
	</div>

	<script>
		var ccube 			= $('#contentCube');
		var radioGroup 		= $('.radio-group');
		var currentClass 	= '';
		$(document).ready(function(){
			changeSide();
		});

		function changeSide(){
			var showClass 	= 'show-' + radioGroup.find(':checked').val();
			if(currentClass){
				ccube.removeClass(currentClass);
			}
			ccube.addClass(showClass);
			currentClass 		= showClass;
		}
		
		radioGroup.on('change',changeSide);
	</script>
</body>
</html>