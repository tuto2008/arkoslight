<?php 
	$v = '?v='.rand(1, 99999);
?>
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>ArkosLight</title>
	<link rel="stylesheet" href="assets/css/jquery.range.css">
	<link rel="stylesheet" href="assets/css/main.css<?php echo $v; ?>">
	<link href="https://fonts.googleapis.com/css?family=Lato:300,400,700,900&amp;subset=latin-ext" rel="stylesheet">
	<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.8.3/jquery.min.js"></script>
	<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
	<script type="text/javascript" src="assets/js/jquery.range.js"></script>
	<script type="text/javascript" src="assets/js/translations.js<?php echo $v; ?>"></script>
	<script type="text/javascript" src="assets/js/bright_line.js<?php echo $v; ?>"></script>
	<script type="text/javascript" src="assets/js/black_foster.js<?php echo $v; ?>"></script>
	<script type="text/javascript" src="assets/js/config.js<?php echo $v; ?>"></script>
	<script type="text/javascript" src="assets/js/simulator.js<?php echo $v; ?>"></script>
	<script type="text/javascript" src="assets/js/main.js<?php echo $v; ?>"></script>
</head>
<body>
	<div class="content">
		<div id="page_selectLine" class="page autoHeight pageActive">
			<div class="wrapPage">
				<h1 data-tr="textSelectProdTitle">{textSelectProdTitle}</h1>
				<ul class="selectLine">
					<li class="item_01"><a href="#" class="disabled" data-type="fifty">Fifty</a></li>
					<li class="item_02"><a href="#" data-type="brightline">Bright Line</a></li>
					<li class="item_03"><a href="#" data-type="blackfoster">Black Foster</a></li>
				</ul>
			</div>
		</div>
		<div class="groupBrightLine">
			<div id="page_start_brightline" class="page autoHeight">
				<div class="wrapPage">
					<h1>Bright Line</h1>
					<h2 data-tr="textProdsSimulatorTitle">{textProdsSimulatorTitle}</h2>
				</div>
				<div class="btnActionWrap">
					<a href="#" id="startBtnBL" class="btnAction" data-page="page_intro_brightline" data-tr="btnContinue">{btnContinue}</a>
				</div>
			</div>
			<div id="page_intro_brightline" class="page autoHeight">
				<div class="wrapPage introText">
					<h1>Bright Line</h1>
					<p data-tr="textBLProdIntro">{textBLProdIntro}</p>
				</div>
				<div class="btnActionWrap">
					<a href="#" id="introBtnReturnBL" class="btnAction" data-page="page_start_brightline" data-tr="btnReturn">{btnReturn}</a>
					<a href="#" id="introBtnBL" class="btnAction" data-page="page_selectSurface_brightline" data-tr="btnContinue">{btnContinue}</a>
				</div>
			</div>
			<div id="page_selectSurface_brightline" class="page autoHeight">
				<div class="wrapPage">
					<h1>Bright Line</h1>
					<h2 data-tr="textBLProdTitleSelectSurface">{textBLProdTitleSelectSurface}</h2>
					<ul class="selectSurfacesGroup">
						<li class="item_01 active"><a href="#" data-type="oneSurface" data-tr="textBLProdTab1Surface">{textBLProdTab1Surface}</a></li>
						<li class="item_02"><a href="#" data-type="twoSurface" data-tr="textBLProdTab2Surface">{textBLProdTab2Surface}</a></li>
						<li class="item_03"><a href="#" data-type="threeSurface" data-tr="textBLProdTab3Surface">{textBLProdTab3Surface}</a></li>
					</ul>
					<form name="selectSurfaceForm" id="selectSurfaceForm">
						<fieldset id="oneSurface" class="active" data-count="1">
							<legend data-tr="textBLProdTab1Surface">{textBLProdTab1Surface}</legend>
							<label class="surface_01"><input type="radio" class="addSurfaceType" data-connect="0000" name="surface" value="singleF"><span data-tr="textBLProdSurfaceSingleF">{textBLProdSurfaceSingleF}</span></label>
							<label class="surface_02"><input type="radio" class="addSurfaceType" data-connect="0000" name="surface" value="singleT"><span data-tr="textBLProdSurfaceSingleT">{textBLProdSurfaceSingleT}</span></label>
						</fieldset>
						<fieldset id="twoSurface" data-count="2">
							<legend data-tr="textBLProdTab2Surface">{textBLProdTab2Surface}</legend>
							<label class="surface_03"><input type="radio" class="addSurfaceType" data-connect="T000|00F0" name="surface" value="multiFT"><span data-tr="textBLProdSurfaceMultiFT">{textBLProdSurfaceMultiFT}</span></label>
							<label class="surface_04"><input type="radio" class="addSurfaceType" data-connect="0F00|000I" name="surface" value="multiIF"><span data-tr="textBLProdSurfaceMultiIF">{textBLProdSurfaceMultiIF}</span></label>
							<label class="surface_05"><input type="radio" class="addSurfaceType" data-connect="0D00|000F" name="surface" value="multiFD"><span data-tr="textBLProdSurfaceMultiFD">{textBLProdSurfaceMultiFD}</span></label>
							<label class="surface_06"><input type="radio" class="addSurfaceType" data-connect="T000|000I" name="surface" value="multiIT"><span data-tr="textBLProdSurfaceMultiIT">{textBLProdSurfaceMultiIT}</span></label>
							<label class="surface_07"><input type="radio" class="addSurfaceType" data-connect="T000|0D00" name="surface" value="multiDT"><span data-tr="textBLProdSurfaceMultiDT">{textBLProdSurfaceMultiDT}</span></label>
							<label class="surface_08"><input type="radio" class="addSurfaceType" data-connect="0000|0000" name="surface" value="multiID"><span data-tr="textBLProdSurfaceMultiID">{textBLProdSurfaceMultiID}</span></label>
						</fieldset>
						<fieldset id="threeSurface" data-count="3">
							<legend data-tr="textBLProdTab3Surface">{textBLProdTab3Surface}</legend>
							<label class="surface_09"><input type="radio" class="addSurfaceType" data-connect="TF00|T00I|00FI" name="surface" value="multiIFT"><span data-tr="textBLProdSurfaceMultiIFT">{textBLProdSurfaceMultiIFT}</span></label>
							<label class="surface_10"><input type="radio" class="addSurfaceType" data-connect="TD00|T00F|0DF0" name="surface" value="multiFDT"><span data-tr="textBLProdSurfaceMultiFDT">{textBLProdSurfaceMultiFDT}</span></label>
							<label class="surface_11"><input type="radio" class="addSurfaceType" data-connect="0F00|0D0I|000F" name="surface" value="multiIFD"><span data-tr="textBLProdSurfaceMultiIFD">{textBLProdSurfaceMultiIFD}</span></label>
						</fieldset>
						<p class="surfaceSelectedText">&nbsp;</p>
					</form>
				</div>
				<div class="btnActionWrap">
					<a href="#" id="selectSurfaceBtnReturnBL" class="btnAction" data-page="page_intro_brightline" data-tr="btnReturn">{btnReturn}</a>
					<a href="#" id="selectSurfaceBtnBL" class="btnAction disabled" data-page="page_simulator" data-tr="btnContinue">{btnContinue}</a>
				</div>
			</div>
		</div>
		<div class="groupBlackFoster">
			<div id="page_start_blackfoster" class="page autoHeight">
				<div class="wrapPage">
					<h1>Black Foster</h1>
					<h2 data-tr="textProdsSimulatorTitle">{textProdsSimulatorTitle}</h2>
				</div>
				<div class="btnActionWrap">
					<a href="#" id="startBtnBF" class="btnAction" data-page="page_intro_blackfoster" data-tr="btnContinue">{btnContinue}</a>
				</div>
			</div>
			<div id="page_intro_blackfoster" class="page autoHeight">
				<div class="wrapPage introText">
					<h1>Black Foster</h1>
					<p data-tr="textBFProdIntro">{textBFProdIntro}</p>
					<a href="#" class="openBrochure">Consultar Brochure</a>
				</div>
				<div class="btnActionWrap">
					<a href="#" id="introBtnReturnBF" class="btnAction" data-page="page_start_blackfoster" data-tr="btnReturn">{btnReturn}</a>
					<a href="#" id="introBtnBF" class="btnAction" data-page="page_selectTemperature_blackfoster" data-tr="btnContinue">{btnContinue}</a>
				</div>
			</div>
			<div id="page_selectTemperature_blackfoster" class="page autoHeight">
				<div class="wrapPage selectByList">
					<h1>Black Foster</h1>
					<h2 data-tr="textBFProdTitleTemperature">{textBFProdTitleTemperature}</h2>
					<form name="selectTemperatureForm" id="selectTemperatureForm" data-variable="temperature">
						<label class="temperature_01">
							<input type="radio" class="addTemperatureType" name="temperature" value="3000"> 3000 K
						</label>
						<label class="temperature_02">
							<input type="radio" class="addTemperatureType" name="temperature" value="4000"> 4000 K
						</label>
					</form>
				</div>
				<div class="btnActionWrap">
					<a href="#" id="temperatureBtnReturnBF" class="btnAction" data-page="page_intro_blackfoster" data-tr="btnReturn">{btnReturn}</a>
					<a href="#" id="temperatureBtnBF" class="btnAction disabled" data-page="page_selectDriver_blackfoster" data-tr="btnContinue">{btnContinue}</a>
				</div>
			</div>
			<div id="page_selectDriver_blackfoster" class="page autoHeight">
				<div class="wrapPage selectByList">
					<h1>Black Foster</h1>
					<h2 data-tr="textBFProdTitleDriver">{textBFProdTitleDriver}</h2>
					<form name="selectDriverForm" id="selectDriverForm" data-variable="driver">
						<label class="driver_01">
							<input type="radio" class="addDriverType" name="driver" value="none"> <span data-tr="textBFProdDriverNone">{textBFProdDriverNone}</span>
						</label>
						<label class="driver_02">
							<input type="radio" class="addDriverType" name="driver" value="push1-10v"> <span data-tr="textBFProdDriver1">{textBFProdDriver1}</span>
						</label>
						<label class="driver_03">
							<input type="radio" class="addDriverType" name="driver" value="dali"> <span data-tr="textBFProdDriver2">{textBFProdDriver2}</span>
						</label>
					</form>
				</div>
				<div class="btnActionWrap">
					<a href="#" id="driverBtnReturnBF" class="btnAction" data-page="page_selectTemperature_blackfoster" data-tr="btnReturn">{btnReturn}</a>
					<a href="#" id="driverBtnBF" class="btnAction disabled" data-page="page_selectKit_blackfoster" data-tr="btnContinue">{btnContinue}</a>
				</div>
			</div>
			<div id="page_selectKit_blackfoster" class="page autoHeight">
				<div class="wrapPage selectByList">
					<h1>Black Foster</h1>
					<h2 data-tr="textBFProdTitleKit">{textBFProdTitleKit}</h2>
					<form name="selectKitForm" id="selectKitForm" data-variable="mountingKit">
						<label class="kit_01">
							<input type="radio" class="addKitType" name="kit" value="kit1"> <span data-tr="textBFProdKit1">{textBFProdKit1}</span>
						</label>
						<label class="kit_02">
							<input type="radio" class="addKitType" name="kit" value="kit2"> <span data-tr="textBFProdKit2">{textBFProdKit2}</span>
						</label>
						<label class="kit_03">
							<input type="radio" class="addKitType" name="kit" value="kit3"> <span data-tr="textBFProdKit3">{textBFProdKit3}</span>
						</label>
						<label class="kit_04">
							<input type="radio" class="addKitType" name="kit" value="kit4"> <span data-tr="textBFProdKit4">{textBFProdKit4}</span>
						</label>
					</form>
				</div>
				<div class="btnActionWrap">
					<a href="#" id="KitBtnReturnBF" class="btnAction" data-page="page_selectTemperature_blackfoster" data-tr="btnReturn">{btnReturn}</a>
					<a href="#" id="KitBtnBF" class="btnAction disabled" data-page="page_simulator" data-tr="btnContinue">{btnContinue}</a>
				</div>
			</div>
		</div>
		<div id="page_simulator" class="page autoHeight">
			<div id="col_01" class="cols autoHeight">
				<select name="surfaceList" id="surfaceList" class="selectList firstElement"></select>
				<div class="measureGroup"></div>
			</div>
			<div id="col_02" class="cols autoHeight">
				<h1>{title}</h1>
				<div class="wrapSimulator">
					<div class="controls">
						<div class="directions">
							<a href="#" data-direction="v" data-start="3" data-tr="btnArrowUp">{btnArrowUp}</a>
							<a href="#" data-direction="h" data-start="4" class="active" data-tr="btnArrowRight">{btnArrowRight}</a>
							<a href="#" data-direction="h" data-start="2" data-tr="btnArrowLeft">{btnArrowLeft}</a>
							<a href="#" data-direction="v" data-start="1" data-tr="btnArrowDown">{btnArrowDown}</a>
						</div>
						<input type="hidden" id="zoomScale" value="100" />
						<fieldset class="btnWrap">
							<a href="#" id="centerSurface" class="action" data-tr="btnCenter">{btnCenter}</a>
							<a href="#" id="toggleMeasure" class="action" data-tr="btnMeasure">{btnMeasure}</a>
						</fieldset>
					</div>
					<p class="errorMsg" data-tr="textBLProdLimitError">{textBLProdLimitError}</p>
					<div id="simulator">
						<div class="wrap"></div>
					</div>
					<div class="navMove">
						<a href="#" id="dragTop" class="item item_01" data-type="top" data-tr="btnArrowUp">{btnArrowUp}</a>
						<a href="#" id="dragRight" class="item item_02" data-type="right" data-tr="btnArrowRight">{btnArrowRight}</a>
						<a href="#" id="dragBottom" class="item item_03" data-type="bottom" data-tr="btnArrowDown">{btnArrowDown}</a>
						<a href="#" id="dragLeft" class="item item_04" data-type="left" data-tr="btnArrowLeft">{btnArrowLeft}</a>
					</div>
					<div class="btnActionWrap">
						<a href="#" id="simulatorBtn" class="btnAction" data-page="page_selectTemperature" data-tr="btnContinue">{btnContinue}</a>
					</div>
				</div>
			</div>
			<div id="col_03" class="cols autoHeight">
				<div>
					<a href="#" class="add_component addBtn firstElement">Componentes</a>
					<div id="listComponentsBtns"></div>
					<select name="itemList" id="itemList" class="selectList">
						<option value="0">Seleccionar componente</option>
					</select>
					<a href="#" class="delete_component btn">Borrar</a>
				</div>
			</div>
		</div>
	</div>
	<div id="modal">
		<div class="contentModal"></div>
	</div>
</body>
</html>