<?php 
	error_reporting(E_ALL &~ E_DEPRECATED);    //hola
	include('application/routes.php');
	include('application/class.TemplatePower.inc.php');
	$tpl 		= new TemplatePower( "application/template.tpl" );
	$tpl->prepare();

	$routes 	= new routes($tpl);
	$tpl->assignGlobal("base_url", 	$routes->getUrl());
	$tpl->assign("v", 				'?v='.rand(1, 99999));
	$tpl->assign("body", 			$routes->getBody());

	$routes->startTpl();
	$routes->printToScreen();

	/*
    $s                  = array(
        'time'       => time(),
        'unic'       => mt_rand(),
        'code'       => 'CN5032H1626C897',
        'secret'     => '209dd60b2ea91fbef784bcbc09c21086',
    );
    $key        = sha1($s['code'].$s['secret'].$s['time'].$s['unic']);
    var_dump($s);
    var_dump($key);
    die();
	*/
?>